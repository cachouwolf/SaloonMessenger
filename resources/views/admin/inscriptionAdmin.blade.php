<!-- HEADER -->
@include ('includes/_header')

<h1>
    Inscription Admin
</h1>
<div class="row">
    <form action="{{route('url_to_registerAdmin')}}" method="post">
        {{csrf_field()}}
        <!-- Clé permettant l'envoie du form -->
        <div class="col-sm-4 col-xs-12 margin_on">
            <div>Nom</div>
            <input type="text" name="lastname" value="">
        </div>
        <div class="col-sm-4 col-xs-12 margin_on">
            <div>Prénom</div>
            <input type="text" name="firstname" value="">
        </div>
        <div class="col-sm-4 col-xs-12 margin_on">
            <div>Pseudo *</div>
            <input type="text" name="pseudo" value="">
        </div>
        <div class="col-sm-4 col-xs-12 margin_on">
            <div>Mail *</div>
            <input type="email" name="mail" value="">
        </div>
        <div class="col-sm-4 col-xs-12 margin_on">
            <div>Mot de passe *</div>
            <input type="password" name="password" value="">
        </div>
        <div class="col-xs-12 margin_on">
            <input type="submit" name="Envoyer" value="Envoyer">
        </div>
    </form>
</div>

<div class="row">
    <div class="col-xs-12">
        * Champs obligatoires
    </div>
    <!-- Vérifie si les champs sont tous remplis -->
    @if (session('erreur_login'))
        <div class="col-xs-12 erreur margin_on" style="color: red;">
            {{ session('erreur_login') }}
        </div>
    @endif
    @if (session('requete'))
        <div class="col-xs-12 erreur margin_on" style="color: red;">
            {{ session('requete') }}
        </div>
    @endif
</div>

<!-- FOOTER -->
@include ('includes/_footer')
