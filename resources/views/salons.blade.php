<!-- HEADER + MENU -->
@include ('includes/_header')

<h1  style="text-align: center;">
    Liste des saloon
</h1>

<!-- Vous êtes bien connecté -->
@if (session('erreur_login'))
    <div class="success" style="color: green;">
        {{ session('erreur_login') }}
    </div>
@endif

@if (session('pseudo'))
    <div class="row hello margin_on">
        Bonjour {{session('pseudo')}} !
        @if(session('role') == 0)
              Vous êtes user !
        @else
            Vous êtes Admin !
        @endif
    </div>
@endif

<div class="row">
        @if(count($salons) == 0)
        <!-- Si pas d'élément dans l'objet salon  -->
            <div class="col-xs-12 margin_on" style="color: #7e7e7e; font-size: 18px; text-align: center;">
                Il n'y a pas de salons disponible pour le moment... :(
            </div>
        @else
        <!-- Sinon, on va faire une boucle pour les retrouver -->
            @foreach($salons as $salon)
                <div class="col-xs-12 salons_name">
                    <a href="{{ route('url_to_selected_salon', $salon->id) }}">
                        {{ $salon -> name }}
                    </a>
                </div>
            @endforeach
        @endif
</div>

<!-- FOOTER -->
@include ('includes/_footer')
