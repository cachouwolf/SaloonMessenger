<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// HOMEPAGE
Route::get('/',
    [
        "as" => "url_to_salon", "uses" => "SalonController@salon_view"
    ]
);

// CONNEXION USER
Route::get('/connexion',
    [
        "as" => "url_to_connexion", "uses" => "ConnexionController@connexion_view"
    ]
);

// VALIDER LES INFOS USER
Route::post('/validatorLogin',
    [
        "as" => "url_to_login", "uses" => "ConnexionController@login"
    ]
);

// INSCRIPTION USER
Route::get('/inscription',
    [
        "as" => "url_to_inscription", "uses" => "InscriptionController@inscription_view"
    ]
);

// ENREGISTREMENT DES DONNEES USER
Route::post('/register',
    [
        "as" => "url_to_register", "uses" => "InscriptionController@register"
    ]
);

// SE DECONNECTER
Route::get('/deconnexion',
    [
        "as" => "url_to_deconnexion", "uses" => "ConnexionController@deconnexion"
    ]
);

// VOIR UN FORUM SPECIFIQUE
Route::get('salon/{id}',
    [
        "as" => "url_to_selected_salon", "uses" => "SalonController@salon_selected"
    ]
);

// ECRIRE UN MESSAGE SUR LE FORUM
Route::post('/sendMessage',
    [
        "as" => "url_to_send_message", "uses" => "MessageController@sendMessage"
    ]
);

// SUPPRMER UN MESSAGE EN AJAX
Route::post('/deleteMessageAjax',
    [
        "as" => "url_to_delete_message_ajax", "uses" => "MessageController@deleteMessageAjax"
    ]
);

// VOIR LE PROFIL USER
Route::get('profile',
    [
        "as" => "url_to_profile", "uses" => "ProfileController@profile"
    ]
);


// INSCRIPTION ADMIN
Route::get('/inscriptionAdmin',
    [
        "as" => "url_to_inscriptionAdmin", "uses" => "InscriptionAdminController@inscriptionAdmin_view"
    ]
);

// ENREGISTREMENT DES DONNEES ADMIN
Route::post('/registerAdmin',
    [
        "as" => "url_to_registerAdmin", "uses" => "InscriptionAdminController@registerAdmin"
    ]
);

// GESTION DES SALONS
Route::get('/gestionSalons',
    [
        "as" => "url_to_gestionSalons", "uses" => "SalonController@gestionSalons"
    ]
);


// MODIFIER UN SALON
Route::post('/modifySalon',
    [
        "as" => "url_to_modify_salon", "uses" => "SalonController@modifySalon"
    ]
);


// SUPPRMER UN SALON
Route::post('/deleteSalon',
    [
        "as" => "url_to_delete_salon", "uses" => "SalonController@deleteSalon"
    ]
);
// SUPPRMER UN SALON EN AJAX
Route::post('/deleteSalonAjax',
    [
        "as" => "url_to_delete_salon_ajax", "uses" => "SalonController@deleteSalonAjax"
    ]
);


// AJOUTER UN SALON
Route::post('/addSalon',
    [
        "as" => "url_to_add_salon", "uses" => "SalonController@addSalon"
    ]
);
