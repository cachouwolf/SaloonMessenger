<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\User;

class ConnexionController extends Controller
{
    public function connexion_view(){
        return view('connexion');
    }

    public function login(Request $rq){
        // On récupère les données
       /* $rq->has('mail');
        $rq->input('mail');
        $rq->has('password');
        $rq->input('password');*/
        //  var_dump($rq->input('password'));die();
        // On vérifie qu'ils ne sont pas vide
        if($rq->has('mail')==false){
            $erreur_login = 'Veuillez remplir tous les champs !';
            return back()->with('erreur_login', $erreur_login);
            // retour vers la même page mais on affiche un message d'erreur
        }else if( $rq->has('password')==false){
            $erreur_login = 'Veuillez remplir tous les champs !';
            return back()->with('erreur_login', $erreur_login);
            // retour vers la même page mais on affiche un message d'erreur
        }else{
        // Verification que le mail soit bien dans la BDD, 2 solutions :
        // - Soit :
            // $requete = DB::table('users')->where('email','=', $rq->input('mail'))->get();
        // - Soit :
            $requete = User::where('email','=', $rq->input('mail'))->first();
            // Affiche le résultat obtenu de la requête :
            // return back()->with('requete', $requete);
            // $user = User::where('password','=', $rq->input('password'))->first();

            if(!empty($requete)){
                $password_verify = password_verify ( $rq->input('password'), $requete->password );

                if($password_verify){
                    $erreur_login = "Vous êtes bien connecté !";

                    session(['pseudo' => $requete->pseudo,
                                'email' => $requete->email,
                                'id_user' => $requete->id,
                                'role' => $requete->role
                            ]);
                    // return redirect()->route('url_to_salon', ['erreur_login' => $erreur_login]);
                    return redirect()->route('url_to_salon')->with('erreur_login', $erreur_login);
                    // redirection vers la homepage mais on affiche un message de succès
                }else{
                    $erreur_login = 'Mot de passe incorrect';
                    return back()->with('erreur_login', $erreur_login);
                }
            }else{
                return redirect()->route('url_to_inscription');
            }
        }
    }

    public function deconnexion(Request $rq){
        $rq->session()->flush();
        $erreur_login = 'Vous êtes déconnecté !';
        return redirect()->route('url_to_salon')->with('erreur_login', $erreur_login);
    }
}
