<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class InscriptionController extends Controller
{
    public function inscription_view(){
        return view('inscription');
    }
    public function register(Request $rq){
        // On récupère les données
       /* $rq->has('firstname');
        $rq->input('firstname');
        $rq->has('lastname');
        $rq->input('lastname');
        $rq->has('pseudo');
        $rq->input('pseudo');
        $rq->has('mail');
        $rq->input('mail');
        $rq->has('password');
        $rq->input('password');*/
        // var_dump($rq); die;
        if($rq->has('mail')==false){
            $erreur_login = 'Veuillez remplir tous les champs !';
            return back()->with('erreur_login', $erreur_login);
            // retour vers la même page mais on affiche un message d'erreur
        }else if( $rq->has('password')==false){
            $erreur_login = 'Veuillez remplir tous les champs !';
            return back()->with('erreur_login', $erreur_login);
            // retour vers la même page mais on affiche un message d'erreur
        }else if( $rq->has('pseudo')==false){
            $erreur_login = 'Veuillez remplir tous les champs !';
            return back()->with('erreur_login', $erreur_login);
            // retour vers la même page mais on affiche un message d'erreur
        }else{
            $requete = User::where('email','=', $rq->input('mail'))->first();
            if(empty($requete)){
                $password = password_hash ( $rq->input('password'),  PASSWORD_DEFAULT);
                $user = new User();
                $user->firstname = $rq->input('firstname');
                $user->lastname =  $rq->input('lastname');
                $user->pseudo = $rq->input('pseudo');
                $user->email =  $rq->input('mail');
                $user->password = $password;
                $user->role = 0;
                $user->save();
               // var_dump($user);die();
                $erreur_login = 'Vous êtes inscrit !';
                return redirect()->route('url_to_salon');
            }else{
                $erreur_login = 'Votre adresse mail existe déjà !';
                return back()->with('erreur_login', $erreur_login);
            }
        }
    }
}
