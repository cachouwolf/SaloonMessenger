<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Salon;
use App\Message;
use App\User;
use Illuminate\Support\Facades\Input;


class SalonController extends Controller
{
    public function salon_view(){
        $salon = Salon::all();
        // var_dump($salon);
        return view('salons')->with('salons', $salon);
    }

    public function salon_selected($id){
        $message = User::join('messages', 'messages.user_id', '=', 'users.id')->where('salon_id','=', $id)->get();
        // var_dump($message[0]);
        $salon = Salon::where('id', '=', $id)->first();
        return view('messages')->with('salons', $salon)->with('messages', $message);

    }

    public function gestionSalons(Request $rq){
        $salon = Salon::all();

        return view('admin.gestionSalons')->with('salons', $salon);
    }

    public function modifySalon(Request $rq){
        $id =  $rq->input('id');
        $name =  $rq->input('name');
        $modifySalon = Salon::where('id', "=", $id)->update(['name' => $name]);
        $salon = Salon::all();

        return view('admin.gestionSalons')->with('salons', $salon);
    }

    public function deleteSalon(Request $rq){
        $id =  $rq->input('id');
        $deleteSalon = Salon::where('id', "=", $id)->delete();
        $salon = Salon::all();

        return view('admin.gestionSalons')->with('salons', $salon);
    }

    public function deleteSalonAjax(Request $rq){
        $dataResponse = array();

        $id = Input::get('id');
        $name = Input::get('name');

        $deleteSalon = Salon::where('id', "=", $id)->delete();

        $dataResponse['deleted'] = true;
        $dataResponse['id'] = $id;
        $dataResponse['name'] = $name;

        return json_encode($dataResponse);
    }


    public function addSalon(Request $rq){
        $name =  $rq->input('addSalon');
        $addSalon = Salon::insert(
                                ['name' => $name]
                            );
        $salon = Salon::all();

        return view('admin.gestionSalons')->with('salons', $salon);
    }
}
